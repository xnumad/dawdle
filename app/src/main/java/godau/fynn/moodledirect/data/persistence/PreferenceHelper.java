package godau.fynn.moodledirect.data.persistence;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Collections;

/**
 * Load shared preferences centrally.
 * Preferences are set automatically by associated key through preference fragment.
 */
public class PreferenceHelper {

    public static final String SETTINGS_FILE = "godau.fynn.moodledirect:preferences";

    private static final String KEY_CURRENT_ACCOUNT = "current_account";
    private static final String KEY_ACCOUNT_LIST = "accounts";

    private final Context context;
    private final SharedPreferences preferences;

    public PreferenceHelper(Context context) {
        this.context = context;
        this.preferences = context.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE);
    }

    public void setUserAccount(UserAccount userAccount) {
        String preferenceValue = getPreferenceFileName(userAccount);
        preferences.edit()
                .putString(KEY_CURRENT_ACCOUNT, preferenceValue)
                .putStringSet(KEY_ACCOUNT_LIST, Collections.singleton(preferenceValue))
                .apply();
    }

    /**
     * Gets currently logged in account.
     */
    public UserAccount getUserAccount() {
        return new UserAccount(context, preferences.getString(KEY_CURRENT_ACCOUNT, null));
    }

    /**
     * @return True if {@link #getUserAccount()} may be called. Does not indicate whether token is valid.
     */
    public boolean isLoggedIn() {
        return preferences.contains(KEY_CURRENT_ACCOUNT);
    }

    public void logout() {
        getUserAccount().logout();
        preferences.edit()
                .remove(KEY_CURRENT_ACCOUNT)
                .putStringSet(KEY_ACCOUNT_LIST, Collections.emptySet())
                .apply();
    }

    private static String getPreferenceFileName(UserAccount userAccount) {
        String domain = userAccount.getUrl().replaceAll("https?://", "")
                .replaceAll("/", ".");
        return domain + ':' + userAccount.getUserId();
    }

    public CourseRowAppearance getCourseRowAppearance() {
        CourseRowAppearance appearance = new CourseRowAppearance();
        appearance.categoryAppearance = preferences.getString("appearance_category", "bottom");
        appearance.showHeaderImage = preferences.getBoolean("appearance_header", true);
        appearance.showUnreadCounter = preferences.getBoolean("unread", true);
        appearance.showDescription = preferences.getBoolean("appearance_description", true);
        return appearance;
    }

    public String getDownloadPath() {
        return preferences.getString("download_path", null);
    }

    public void setDownloadPath(String uri) {
        preferences.edit()
                .putString("download_path", uri)
                .apply();
    }

    /**
     * Do not use yet, as notifications have not yet been implemented
     */
    public boolean isNotificationsEnabled() {
        return preferences.getBoolean("notifications", false);
    }

    /**
     * @return Whether user has requested offline mode to always be enabled.
     */
    public boolean isForceOfflineModeEnabled() {
        return preferences.getBoolean("offline", false);
    }

    public boolean isDarkThemeEnabled() {
        return preferences.getBoolean("dark_theme", false);
    }

    public boolean isAutoLoginEnabled() {
        return preferences.getBoolean("web_auto_login", true);
    }

    public void setLastHelpMenuEntry(String entry) {
        preferences.edit().putString("help_entry", entry).apply();
    }

    public String getLastHelpMenuEntry() {
        return preferences.getString("help_entry", "");
    }

    public static class CourseRowAppearance {
        public String categoryAppearance;
        public boolean showHeaderImage, showUnreadCounter, showDescription;
    }
}
