package godau.fynn.moodledirect.data.persistence;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import godau.fynn.moodledirect.model.api.SiteInformation;
import godau.fynn.moodledirect.module.link.ChoiceLink;
import godau.fynn.moodledirect.util.ConfigDownloadHelper;

import static android.content.Context.MODE_PRIVATE;

import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class UserAccount {

    private final Context context;
    private final String fileName;
    private final SharedPreferences preferences;

    UserAccount(Context context, String file) {
        this.context = context;
        this.fileName = file;
        preferences = context.getSharedPreferences(file, MODE_PRIVATE);
    }

    /**
     * Initializes UserAccount based on data downloaded after logging in.
     * It is the caller's responsibility to call {@link PreferenceHelper#setUserAccount(UserAccount)}.
     */
    public UserAccount(Context context, SiteInformation siteInformation) {
        this(context, siteInformation.getSiteUrl().replaceAll("https?://", "")
                .replaceAll("/", ".") + ':' + siteInformation.userId);

        from(siteInformation);

        // Additionally extract token (not serialized)
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("token", siteInformation.token.token);
        if (siteInformation.token.privatetoken != null) {
            editor.putString("token_private", siteInformation.token.privatetoken);
        }
        editor.apply();

    }

    public void from(SiteInformation siteInformation) {
        SharedPreferences.Editor editor = preferences.edit();
        editor
                .putString("username", siteInformation.username)
                .putString("private_access_key", siteInformation.privateAccessKey)
                .putString("name_first", siteInformation.firstName)
                .putString("name_last", siteInformation.lastName)
                .putString("avatar", siteInformation.avatar)
                .putString("url", siteInformation.getSiteUrl())
                .putInt("id", siteInformation.userId);

        // Copy supported calls
        Set<String> supportedCalls = new HashSet<>();
        for (SiteInformation.Function function : siteInformation.supportedCalls) {
            supportedCalls.add(function.name);
        }
        editor.putStringSet("instance_supported_calls", supportedCalls);
        Log.d(UserAccount.class.getSimpleName(), "Stored " + supportedCalls.size() + " " +
                "supported function calls");

        editor.apply();
    }

    public String getToken() {
        return preferences.getString("token", "");
    }

    public boolean hasPrivateToken() {
        return preferences.contains("token_private");
    }

    public String getPrivateToken() {
        return preferences.getString("token_private", null);
    }

    public String getFilePrivateAccessKey() {
        return preferences.getString("private_access_key", "");
    }

    public String getUserLoginName() {
        return preferences.getString("username", "");

    }

    public String getUserFirstName() {
        return preferences.getString("name_first", "");

    }

    public int getUserId() {
        return preferences.getInt("id", 0);

    }

    /**
     * Gets API URL (in a normalized form starting with <code>http[s]://</code> and
     * ending with a trailing <code>/</code>).
     */
    public String getUrl() {
        return preferences.getString("url", "");
    }

    public String getMoodleInstanceName() {
        return preferences.getString("instance_name", "");
    }

    public void setMoodleInstanceName(String u) {
        preferences.edit()
                .putString("instance_name", u)
                .apply();
    }

    public String getAvatarUrl() {
        return preferences.getString("avatar", "");
    }

    public String getDatabaseName() {
        return fileName;
    }

    public void setAutoLoginTime() {
        preferences.edit()
                .putLong("time_last_autologin", System.currentTimeMillis())
                .apply();
    }

    public long getLastAutoLoginTime() {
        return preferences.getLong("time_last_autologin", 0);
    }

    public void setAutoLoginCooldown(long cooldown) {
        preferences.edit()
                .putLong("autologin_cooldown", cooldown)
                .apply();
    }

    public long getAutoLoginCooldown() {
        return preferences.getLong("autologin_cooldown", 360 * 1000L);
    }

    public void setSupportedCalls(Set<String> supportedCalls) {
        preferences.edit()
                .putStringSet("instance_supported_calls", supportedCalls)
                .apply();
    }

    public @Nullable Set<String> getSupportedCalls() {
        return preferences.getStringSet("instance_supported_calls", null);
    }

    @SuppressLint("ApplySharedPref")
    public void logout() {
        preferences.edit().clear().commit();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.deleteSharedPreferences(fileName);
        }
    }
}
