package godau.fynn.moodledirect.network;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import godau.fynn.moodledirect.BuildConfig;
import godau.fynn.moodledirect.util.Constants;
import okhttp3.Dns;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by SKrPl on 11/23/17.
 */

public class APIClient {

    private static MoodleServices services;

    private APIClient() {
    }

    public static MoodleServices getServices() {

        if (services == null) {
            services = makeServices(Constants.API_URL);
        }

        return services;
    }

    public static MoodleServices makeServices(String instance) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.dns(hostname -> {
            List<InetAddress> addresses = new ArrayList<>(Dns.SYSTEM.lookup(hostname));
            Collections.sort(addresses, (o1, o2) -> Boolean.valueOf(o1 instanceof Inet6Address).compareTo(o2 instanceof Inet6Address));
            return addresses;
        });


        builder.addInterceptor(new Validator());

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(instance)
                .client(builder.build())
                .build();

        return retrofit.create(MoodleServices.class);
    }

    public static void clearMoodleInstance() {
        services = null;
    }
}
