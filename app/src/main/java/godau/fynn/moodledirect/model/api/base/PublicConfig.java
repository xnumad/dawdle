package godau.fynn.moodledirect.model.api.base;

import java.io.Serializable;

public class PublicConfig implements Serializable {
    public String sitename;
    public int typeoflogin;
    public String launchurl;
    private String httpswwwroot;

    public void setHttpswwwroot(String httpswwwroot) {
        this.httpswwwroot = httpswwwroot;
    }

    /**
     * Guarantees that {@code httpswwwroot} ends with a trailing '/', unless it is null.
     */
    public String getHttpswwwroot() {
        if (httpswwwroot == null) return null;
        if (httpswwwroot.endsWith("/")) return httpswwwroot;
        else return httpswwwroot + "/";
    }
}
