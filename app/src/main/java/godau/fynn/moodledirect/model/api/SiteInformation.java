package godau.fynn.moodledirect.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SiteInformation {

    /**
     * Login name, not to be confused with real name
     */
    public String username;

    /**
     * First name of the user
     */
    @SerializedName("firstname")
    public String firstName;

    /**
     * Last name of the user
     */
    @SerializedName("lastname")
    public String lastName;

    /**
     * URL to user's profile picture
     */
    @SerializedName("userpictureurl")
    public String avatar;

    /**
     * Numeric user ID
     */
    @SerializedName("userid")
    public int userId;

    /**
     * Not serialized
     */
    public UserToken token;

    @SerializedName("userprivateaccesskey")
    public String privateAccessKey;

    @SerializedName("siteurl")
    private String siteUrl;

    @SerializedName("functions")
    public List<Function> supportedCalls;

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public String getSiteUrl() {
        return siteUrl.endsWith("/")? siteUrl : siteUrl + "/";
    }

    public static class Function {
        public String name, version;
    }
}