package godau.fynn.moodledirect.model.api.choice;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import com.google.gson.annotations.SerializedName;
import godau.fynn.moodledirect.model.api.file.File;
import godau.fynn.moodledirect.module.files.HasFiles;

import java.util.List;

@Entity(tableName = "choice")
public class ChoiceDetails implements HasFiles {

    /**
     * Indicates that choice results are hidden.
     */
    public static final int RESULT_AVAILABILITY_NEVER = 0;

    /**
     * Indicates that choice results are made available after answering.
     */
    public static final int RESULT_AVAILABILITY_AFTER_ANSWER = 1;

    /**
     * Indicates that choice results are available as soon as the choice closes.
     */
    public static final int RESULT_AVAILABILITY_AFTER_CLOSE = 2;

    /**
     * Indicates that choice results are permanently available.
     */
    public static final int RESULT_AVAILABILITY_ALWAYS = 3;

    @PrimaryKey
    public int id;

    public String name;

    @SerializedName("intro")
    public String description;

    @Ignore
    @SerializedName("introfiles")
    public List<File> descriptionFiles;

    @SerializedName("allowupdate")
    public boolean allowUpdate;

    @SerializedName("allowmultiple")
    public boolean allowMultiple;

    @SerializedName("limitanswers")
    public boolean limitAnswers;

    /**
     * Code that indicates when results will be available, if ever. See
     * {@link #RESULT_AVAILABILITY_NEVER}, {@link #RESULT_AVAILABILITY_AFTER_ANSWER},
     * {@link #RESULT_AVAILABILITY_AFTER_CLOSE} and {@link #RESULT_AVAILABILITY_ALWAYS}.
     */
    @SerializedName("showresults")
    @ColumnInfo(defaultValue = "0")
    public int resultAvailability;

    /**
     * True if choices are onymous, false if they are anonymous.
     */
    @SerializedName("publish")
    @ColumnInfo(defaultValue = "false")
    public boolean resultOnymous;

    /**
     * Timestamp in seconds at which this poll becomes available. 0 if unset.
     */
    @SerializedName("timeopen")
    @ColumnInfo(defaultValue = "0")
    public long timeOpen;

    /**
     * Timestamp in seconds from which on this poll becomes unavailable again. 0 if unset.
     */
    @SerializedName("timeclose")
    @ColumnInfo(defaultValue = "0")
    public long timeClose;

    // === HASFILES IMPLEMENTATION ===

    @Override
    public List<File> getFileList() {
        return descriptionFiles;
    }

    @Override
    public void setFileList(List<File> fileList) {
        descriptionFiles = fileList;
    }

    @Override
    public String getReference() {
        return "choice" + id;
    }
}
