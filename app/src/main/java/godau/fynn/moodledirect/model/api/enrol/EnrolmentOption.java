package godau.fynn.moodledirect.model.api.enrol;

import com.google.gson.annotations.SerializedName;

public class EnrolmentOption {
    public int id;

    @SerializedName("courseid")
    public int courseId;

    public String type, name, wsfunction, status;
}
