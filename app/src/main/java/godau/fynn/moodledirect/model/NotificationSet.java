package godau.fynn.moodledirect.model;

import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.CourseSection;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.model.api.forum.Discussion;

/**
 * Created by harsu on 18-01-2017.
 */

public class NotificationSet {

    private int uniqueId;
    private int bundleId;
    private String notifSummary;
    private String notifTitle;
    private String notifContent;

    public NotificationSet() {
    }

    public NotificationSet(int uniqueId, int bundleId, String notifTitle, String notifContent, String notifSummary) {
        this.uniqueId = uniqueId;
        this.bundleId = bundleId;
        this.notifTitle = notifTitle;
        this.notifContent = notifContent;
        this.notifSummary = notifSummary;
    }

    /** Helper methods to create NotificationSet objects */
    public static NotificationSet createNotificationSet(Course course, CourseSection section, Module module) {
        return new NotificationSet(module.id, course.getId(), section.getName(), module.getName(), course.shortname);
    }

    public static NotificationSet createNotificationSet(Course course, Module module, Discussion discussion) {
        return new NotificationSet(discussion.id, course.getId(), module.getName(), discussion.message, course.shortname);
    }

    public static NotificationSet createNotificationSet(int courseID, String courseName, int modId, String moduleName) {
        return new NotificationSet(modId, courseID, "", moduleName, courseName);
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public int getBundleID() {
        return bundleId;
    }


    public void setBundleID(int bundleID) {
        this.bundleId = bundleID;
    }

    public String getNotifSummary() {
        return notifSummary;
    }

    public void setNotifSummary(String notifSummary) {
        this.notifSummary = notifSummary;
    }

    public String getNotifTitle() {
        return notifTitle;
    }

    public void setNotifTitle(String notifTitle) {
        this.notifTitle = notifTitle;
    }

    public String getNotifContent() {
        return notifContent;
    }

    public void setNotifContent(String notifContent) {
        this.notifContent = notifContent;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getTitle() {
        return notifSummary;
    }

    public String getGroupKey() {
        return notifSummary;

    }

}
