package godau.fynn.moodledirect.model.api.choice;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "choiceOption")
public class ChoiceOption {

    @PrimaryKey
    public int id;

    /**
     * Maximum number of users that may select this answer
     */
    @SerializedName("maxanswers")
    public int limit;

    /**
     * Current amount of users that selected this answer
     */
    @SerializedName("countanswers")
    public int answerCount;
    public String text;
    public boolean checked, disabled;

    // === DATABASE ===
    public int reference;
    @ColumnInfo(defaultValue = "0")
    public int orderNumber;
}
