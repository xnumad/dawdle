package godau.fynn.moodledirect.model.api.choice;

import java.util.List;

/**
 * Only for {@code Choice.getChoiceDetails}!
 */
public class ChoiceDetailsList {
    public List<ChoiceDetails> choices;
}
