package godau.fynn.moodledirect.model.database;

import android.content.Context;
import android.content.res.Resources;
import android.text.Html;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.Ignore;
import com.google.gson.annotations.SerializedName;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.ResourceType;
import godau.fynn.moodledirect.model.api.file.File;
import godau.fynn.moodledirect.module.FileManager;
import godau.fynn.moodledirect.module.files.HasFiles;
import godau.fynn.moodledirect.util.TextUtil;
import godau.fynn.moodledirect.view.ExpandableTextDisplay;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by harsu on 16-12-2016.
 */

/**
 * Primary key consists of {@code id} plus {@code modname} because course
 * sections have different IDs.
 */
@Entity(tableName = "module", primaryKeys = {"id", "moduleType"})
public class Module implements HasFiles {

    public int id;
    public String url;

    /**
     * HTML-encoded name (therefore protected to prevent accidental access to encoded version)
     */
    protected String name;
    public int instance;

    /**
     * String identifier of the kind of module that this object represents.
     *
     * Access with {@link #getModuleType()} preferred.
     */
    @SerializedName("modname")
    public @NonNull String moduleType;

    /**
     * URL of an icon that resembles this kind of module.
     */
    @SerializedName("modicon")
    public String moduleIconUrl;

    /**
     * HTML text indicating why an activity is momentarily not available
     */
    @SerializedName("availabilityinfo")
    public @Nullable String notAvailableReason;

    @SerializedName("uservisible")
    public boolean available;

    @SerializedName(value = "description", alternate = {"summary"})
    public String description;

    // === DATABASE ===

    public int courseId,
            orderNumber;

    // === NOT IN DATABASE ===

    @Ignore
    public List<File> contents;

    @Ignore
    private ExpandableTextDisplay expandableTextDisplay;

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Module && ((Module) obj).id == id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ResourceType getModuleType() {
        return ResourceType.getTypeFromIdentifier(moduleType);
    }

    public String getName() {
        return Html.fromHtml(name).toString();
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getInstance() {
        return instance;
    }

    public void setInstance(int instance) {
        this.instance = instance;
    }

    public ExpandableTextDisplay getExpandableTextDisplay(Context renderContext, int width) {
        if (expandableTextDisplay == null) {
            expandableTextDisplay = new ExpandableTextDisplay();
            expandableTextDisplay.text = TextUtil.fromHtml(getDescription(), renderContext, width);
            if (getFileList() != null) {
                expandableTextDisplay.files = getFileList().stream()
                        .map(file -> file.url).collect(Collectors.toList());
            } else {
                expandableTextDisplay.files = Collections.emptyList();
            }
        }
        return expandableTextDisplay;
    }

    /**
     * @return resource id if icon available, else returns <code>Resources.ID_NULL</code>
     */
    public int getModuleIcon() {

        switch (getModuleType()) {
            case FILE:
                if (contents == null || contents.isEmpty()) return Resources.ID_NULL;
                return FileManager.getIconFromFileName(contents.get(0).filename);

            case QUIZ:
                return (R.drawable.quiz);

            default:
                return Resources.ID_NULL;
        }
    }


    public boolean isDownloadable() {
        return contents != null && (
            contents.size() == 1 && getModuleType() == ResourceType.FILE
                || contents.size() > 0 && getModuleType() == ResourceType.FOLDER
        );
    }

    // === HASFILE IMPLEMENTATION ===
    public List<File> getFileList() {
        return contents;
    }

    @Override
    public void setFileList(List<File> fileList) {
        contents = fileList;
    }

    @Override
    public String getReference() {
        return String.valueOf(id);
    }
}
