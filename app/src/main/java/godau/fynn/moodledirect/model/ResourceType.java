package godau.fynn.moodledirect.model;

public enum ResourceType {

    // Activities
    ASSIGNMENT("assign"),
    CHOICE("choice"),
    FORUM("forum"),
    QUIZ("quiz"),

    // Resources
    FILE("resource"),
    FOLDER("folder"),
    LABEL("label"),
    PAGE("page"),
    URL("url"),
    ZOOM("zoom"),


    UNKNOWN("");

    public final String id;

    ResourceType(String identifier) {
        id = identifier;
    }

    public static ResourceType getTypeFromIdentifier(String id) {
        for (ResourceType type :
                ResourceType.values()) {
            if (
                    type.id.equalsIgnoreCase(id)
            ) return type;
        }

        return UNKNOWN;
    }
}
