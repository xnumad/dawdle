package godau.fynn.moodledirect.model.api.forum;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import godau.fynn.moodledirect.model.api.file.File;
import godau.fynn.moodledirect.module.files.HasFiles;

@Entity(tableName = "discussion")
public class Discussion implements HasFiles {

    /**
     * Unique ID among all forums for this <b>post</b>.
     */
    @PrimaryKey
    public int id;

    /**
     * Name of this discussion
     */
    public String name;

    // public int groupid; // presumably for group discussions

    /**
     * Unique ID among all forums for this <b>discussion</b>.
     */
    @SerializedName(value = "discussion", alternate = "discussionid")
    public int discussionId;

    /**
     * 0 if root post, parent's discussion ID otherwise.
     */
    @SerializedName(value = "parent", alternate = "parentid")
    public int parent;

    /**
     * ID of user who created this post.
     */
    @SerializedName("userid")
    public int userId;

    /**
     * Name of the user who created this post.
     */
    @SerializedName("userfullname")
    public String userName;

    /**
     * Avatar of the user who created this post.
     */
    @SerializedName("userpictureurl")
    public String userAvatar;

    /**
     * Title of this post.
     */
    public String subject;

    /**
     * Content of this post, in HTML format.
     */
    public String message;

    /**
     * Time that this post was created.
     */
    @SerializedName(value = "created", alternate = "timecreated")
    public int created;

    /**
     * Time that this post was last modified. Note that moodle by default only allows edits shortly
     * after the post was initially posted.
     */
    @SerializedName(value = "modified", alternate = "timemodified")
    public int modified;

    /**
     * List of attachments.
     */
    @Ignore
    public List<File> attachments;

    //public int totalscore;
    //public int mailnow;

    // === FIELDS that are ONLY AVAILABLE when querying a LIST OF DISCUSSIONS ===
    //  == new such fields must be added to usages of DiscussionMergeHelper   ==

    /*
     * Time that the latest post in this discussion was posted. Only available when querying a list of discussions.
     *
     * Currently not included due to name conflict with timemodified from mod_forum_get_forum_discussions.
     */
    //@SerializedName("timemodified")
    //public int lastPostTime;

    /**
     * User by whom the latest post in this discussion was posted. Only available when querying a list of discussions.
     */
    @SerializedName("usermodified")
    public int lastPostUserId;

    /**
     * Number of replies, not including the root post. Only available when querying a list of discussions.
     */
    @SerializedName("numreplies")
    public int replyCount;

    /**
     * True if this discussion is pinned. Only available when querying a list of discussions.
     */
    public boolean pinned;

    // === DATABASE FIELDS ===

    /**
     * Not deserialized but injected later. ID of the forum that this discussion item was found in.
     * Is set if and only if this is a root discussion item.
     */
    public int forumId;

    // ?? public int numunread;

    // === RUNTIME & DESERIALIZATION FIELDS ===

    /**
     * Hierarchical depth of this item. Calculated at runtime by considering the parent value of this
     * and other items.
     */
    @Ignore
    public int depth;

    @Ignore
    public AuthorData author;

    // === HASFILES IMPLEMENTATION ===

    @Override
    public List<File> getFileList() {
        return attachments;
    }

    @Override
    public void setFileList(List<File> fileList) {
        this.attachments = fileList;
    }

    @Override
    public String getReference() {
        return "discussion" + discussionId;
    }
}
