package godau.fynn.moodledirect.model.api;

import java.io.IOException;

public class MoodleException extends IOException {

    public final String errorcode;
    public final String message;

    public MoodleException(String errorcode, String message) {
        super(message);
        this.errorcode = errorcode;
        this.message = message;
    }

    public ErrorCode getErrorCode() {
        for (ErrorCode e : ErrorCode.values()) {
            if (errorcode.equals(e.apiCode)) return e;
        }
        return ErrorCode.UNKNOWN;
    }

    public enum ErrorCode {
        INVALID_TOKEN("invalidtoken"),
        INVALID_LOGIN("invalidlogin"),
        INVALID_PARAMETER("invalidparameter"),
        MISSING_PARAMETER("missingparam"),
        FILE_NOT_FOUND("filenotfound"),
        ACCESS_EXCEPTION("accessexception"),
        SITE_MAINTENANCE("sitemaintenance"),
        SITE_CONFIGURATION_NO_DATABASE_CONNECTION("dbconnectionfailed"),
        SERVICE_UNAVAILABLE("servicenotavailable"),
        WEB_SERVICE_NOT_ENABLED("enablewsdescription"),
        INVALID_PRIVATE_TOKEN("invalidprivatetoken"),
        AUTO_LOGIN_TIME_LIMITED("autologinkeygenerationlockout"),
        UNKNOWN;

        final String apiCode;

        ErrorCode() {
            apiCode = null;
        }

        ErrorCode(String apiCode) {
            this.apiCode = apiCode;
        }

    }
}
