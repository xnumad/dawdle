package godau.fynn.moodledirect.util;

import android.content.Context;
import android.content.Intent;

import java.util.Locale;

import godau.fynn.librariesdirect.AboutDirectActivity;
import godau.fynn.librariesdirect.model.*;
import godau.fynn.moodledirect.BuildConfig;
import godau.fynn.moodledirect.R;

public class AboutDirectHelper {

    private static final String OWN_LICENSE_FOREWORD = "This project, as a whole, is licensed under the terms of the GNU GPL v3 or later. However, to allow the maintainers of the upstream project to benefit from my work, I, Fynn Godau, the copyright owner of most exclusively GPL-licensed content that this project contains, grant the following exceptions per section 7 of the GNU GPL:\n" +
            "\n" +
            "– The work covered by this license may be added to the main CMS-Android project, in a way that integrates into its user experience, under the terms of the MIT license.\n" +
            "– The work covered by this license may be used as a basis for future versions of the main CMS-Android project under the terms of the MIT license.\n" +
            "– The main CMS-Android project refers to the repository located at https://github.com/crux-bphc/CMS-Android/.\n" +
            "– By anyone who intends to contribute to the main CMS-Android project, the work covered by this license may be modified and adapted for the purpose of achieving the above two goals. This includes, among others, copying material into a fork of the aforementioned project, working on this copy and opening a pull request.\n" +
            "– It follows that once any source code is copied into the main CMS-Android project, that source code, in the potentially adapted form in which it is added to that project, shall be governed by the terms of the MIT license. However, other portions of the moodleDirect project are not affected and continue to be made available under this license.\n" +
            "\n" +
            "Many thanks to the authors of the CMS-Android project for their free software work that this project is based on.";

    private static final License HTMLCLEANER_3BSD = new License("3-clause BSD", "Copyright (c) 2006-2022, HtmlCleaner team.\n" +
            "All rights reserved.\n" +
            "\n" +
            "Redistribution and use of this software in source and binary forms, \n" +
            "with or without modification, are permitted provided that the \n" +
            "following conditions are met:\n" +
            "\n" +
            "* Redistributions of source code must retain the above\n" +
            "  copyright notice, this list of conditions and the\n" +
            "  following disclaimer.\n" +
            "\n" +
            "* Redistributions in binary form must reproduce the above\n" +
            "  copyright notice, this list of conditions and the\n" +
            "  following disclaimer in the documentation and/or other\n" +
            "  materials provided with the distribution.\n" +
            "\n" +
            "* The name of HtmlCleaner may not be used to endorse or promote\n" +
            "  products derived from this software without specific prior\n" +
            "  written permission.\n" +
            "\n" +
            "THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \n" +
            "\"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT \n" +
            "LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR \n" +
            "A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT \n" +
            "OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, \n" +
            "SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT \n" +
            "LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, \n" +
            "DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY \n" +
            "THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT \n" +
            "(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE \n" +
            "OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.");

    public static AboutDirectActivity.IntentBuilder getBuilder(Context context) {
        return new AboutDirectActivity.IntentBuilder(context, R.string.app_name, BuildConfig.VERSION_NAME)
                .setAppDeveloperName("Fynn Godau")
                .setAppDeveloperMastodon(context.getString(R.string.link_mastodon))
                .setIcon(R.mipmap.ic_launcher)
                .setContent(new Object[]{
                        new OwnLicense(License.GNU_GPL_V3_OR_LATER_LICENSE, OWN_LICENSE_FOREWORD, "https://codeberg.org/fynngodau/dawdle"),
                        new Fork("CMS-Android", License.MIT_LICENSE, null, "Crux", "https://github.com/crux-bphc/CMS-Android"),
                        new Contribution(License.MIT_LICENSE, null, "Isa f2k1de", null),
                        new Contribution(License.GNU_GPL_V3_LICENSE, null, "Septem9er", null),
                        new Contribution(License.GNU_GPL_V3_OR_LATER_LICENSE, null, "xnumad", null),
                        new Artwork("ICSx⁵", License.GNU_GPL_V3_LICENSE, null, "bitfire web engineering GmbH", true, "https://icsx5.bitfire.at/"),
                        new Translator("Fjuro", null, new Locale("cs")),
                        new Translator("Isabelle K", null, new Locale("de")),
                        new Translator("eUgEntOptIc44", null, new Locale("de")),
                        new Translator("vollkorntomate", null, new Locale("de")),
                        new Translator("Adolfo Jayme Barrientos", null, new Locale("es")),
                        new Translator("fito", null, new Locale("es")),
                        new Translator("girlinblack", null, new Locale("es")),
                        new Translator("Altons", null, new Locale("fr")),
                        new Translator("lejun", null, new Locale("fr")),
                        new Translator("mondstern", null, new Locale("nl")),
                        new Translator("Timur", null, new Locale("ru")),
                        new Translator("dikey0ficial", null, new Locale("ru")),
                        new Translator("mondstern", null, new Locale("ru")),
                        new Translator("Степан", null, new Locale("ru")),
                        new Translator("metezd", null, new Locale("tr")),
                        new Library("Retrofit", License.APACHE_20_LICENSE, null, "Square, Inc.", "https://square.github.io/retrofit/"),
                        new Library("OkHttp", License.APACHE_20_LICENSE, null, "Square, Inc.", "https://square.github.io/okhttp/"),
                        new Library("Picasso", License.APACHE_20_LICENSE, null, "Square, Inc.", "https://square.github.io/picasso/"),
                        new Library("GlideToVectorYou", License.APACHE_20_LICENSE, null, "Corouteam", "https://github.com/corouteam/GlideToVectorYou"),
                        new Library("HtmlCleaner", HTMLCLEANER_3BSD, null, "HtmlCleaner team", "http://htmlcleaner.sourceforge.net/"),
                        new Library("SDHtmlTextView", License.APACHE_20_LICENSE, null, "SysdataSpA", false, "https://github.com/SysdataSpA/SDHtmlTextView"),
                        new Library("Room", License.APACHE_20_LICENSE, null, "AOSP", false, "https://developer.android.com/jetpack/androidx/releases/room"),
                        new Library("TypedRecyclerView", License.CC0_LICENSE, null, "Fynn Godau", "https://codeberg.org/fynngodau/TypedRecyclerView"),
                        new Library("aboutDirect", License.CC0_LICENSE, null, "Fynn Godau", "https://codeberg.org/fynngodau/librariesDirect"),
                });
    }
}
