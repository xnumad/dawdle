package godau.fynn.moodledirect.util;

import android.content.Context;
import android.util.Log;

import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.data.persistence.UserAccount;
import godau.fynn.moodledirect.model.api.tool.MobileConfig;

public class ConfigDownloadHelper {

    /**
     * Updates all information that is stored in the user's account
     *
     * @param dispatch Dispatch to be used. Note that an offline dispatch will result in no action.
     */
    public static void updateSiteInformation(MoodleDatabase.Dispatch dispatch, Context context) {
        PreferenceHelper preferences = new PreferenceHelper(context);
        UserAccount userAccount = preferences.getUserAccount();

        // Stores updated data, like name or avatar, to preferences
        ExceptionHandler.tryAndThenThread(
                () -> dispatch.getCore().getSiteInformation(),
                userAccount::from,
                context
        );
    }

    public static void updateAutoLoginCooldown(Context context) {

        PreferenceHelper preferences = new PreferenceHelper(context);
        UserAccount userAccount = preferences.getUserAccount();

        ExceptionHandler.tryAndThenThread(
                () -> MyApplication.moodle().getDispatch().getTool().getMobileConfig(),
                mobileConfig -> {

                    // Find autologin minimum cooldown time
                    // If not found, Account assumes the default value of 6 minutes
                    for (MobileConfig.Pair pair : mobileConfig.settings) {
                        if ("tool_mobile_autologinmintimebetweenreq"
                                .equals(pair.name)) {
                            userAccount.setAutoLoginCooldown(Long.parseLong(pair.value) * 1000L);
                            Log.d(ConfigDownloadHelper.class.getSimpleName(), "Autologin cooldown updated to " + pair.value + " seconds");
                            return;
                        }
                    }
                },
                context
        );
    }
}
