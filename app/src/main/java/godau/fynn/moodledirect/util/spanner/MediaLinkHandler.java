package godau.fynn.moodledirect.util.spanner;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.text.style.URLSpan;
import com.sysdata.htmlspanner.SpanStack;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.util.Constants;
import godau.fynn.moodledirect.util.Util;
import org.htmlcleaner.TagNode;

/**
 * For some obscene reason, video embeds are sent as pure links in some circumstances unless calls are sent via
 * <code>tool_mobile_call_external_functions</code>. Here we detect links that lead to
 * <code>webservice/pluginfile.php</code> and redirect them to <code>tokenpluginfile.php/$private_access_key</code>.
 */
public class MediaLinkHandler extends com.sysdata.htmlspanner.handlers.LinkHandler {

    private final Context context;

    public MediaLinkHandler(Context context) {
        this.context = context;
    }

    @Override
    public void handleTagNode(TagNode node, SpannableStringBuilder builder, int start, int end, SpanStack spanStack) {

        String url = node.getAttributeByName("href");
        // Analogous to ImageLoaderTextView
        if (url != null) if (url.startsWith(Constants.API_URL + "webservice/pluginfile.php") || url.startsWith(Constants.API_URL + "pluginfile.php")) {
            // Handle multimedia embed

            // Show play icon if no images are contained within link
            boolean showIcon = node.findElementByName("img", true) == null;

            // Media link
            String privateAccessKey = new PreferenceHelper(context).getUserAccount().getFilePrivateAccessKey();
            url = url
                    .replace("/webservice/pluginfile.php/", "/pluginfile.php/")
                    .replace("/pluginfile.php/",
                            "/tokenpluginfile.php/" +
                                    privateAccessKey + "/"
                    );

            if (showIcon) {
                // Play icon
                builder.insert(start, "\uFFFC ");
                ImageSpan imageSpan = new ImageSpan(loadDrawable());
                spanStack.pushSpan(imageSpan, start, start + 1);

                spanStack.pushSpan(new URLSpan(url), start, start + 1);
                start = start + 2;
            }


            spanStack.pushSpan(new URLSpan(url), start, builder.length());


        } else {
            super.handleTagNode(node, builder, start, end, spanStack);
        }
    }

    public Drawable loadDrawable() {
        Drawable drawable = context.getDrawable(R.drawable.ic_play);
        int size = Util.spToPx(16, context);
        drawable.setBounds(0, 0, size, size);
        return drawable;
    }
}
