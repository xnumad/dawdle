package godau.fynn.moodledirect.util.spanner;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.text.Spannable;
import android.util.Log;
import android.widget.TextView;

import com.sysdata.htmlspanner.TagNodeHandler;
import com.sysdata.htmlspanner.handlers.StyledTextHandler;
import com.sysdata.htmlspanner.handlers.TableHandler;
import com.sysdata.htmlspanner.style.Style;

import godau.fynn.moodledirect.util.Util;

public class HtmlSpanner extends com.sysdata.htmlspanner.HtmlSpanner {

    private final Context context;
    private final int textColor;

    private HtmlSpanner(Context context, TextView textView, int width) {
        super(textView.getCurrentTextColor(), textView.getTextSize());
        this.context = context;
        this.textColor = textView.getCurrentTextColor();
        init(width);
    }

    public static HtmlSpanner get(Context context, int width) {
        return new HtmlSpanner(context, new TextView(context), width);
    }

    private void init(int width) {
        setStripExtraWhiteSpace(true);
        registerHandler("img", new ImageHandler(context));
        registerHandler("iframe", new IframeHandler(context));
        registerHandler("a", new MediaLinkHandler(context));
        registerHandler("video", new VideoHandler(context));
        registerHandler("hr", new HorizontalLineHandler(
                wrap(new StyledTextHandler(new Style().setDisplayStyle(Style.DisplayStyle.BLOCK))),
                context
        ));

        TableHandler tableHandler = new TableHandler();
        Log.d("Width", ""+context.getResources().getDisplayMetrics().widthPixels);
        width -= Util.dpToPx(32, context);
        if (width <= 1) width = 300; // some fallback width
        tableHandler.setTableWidth(
                width
        );
        tableHandler.setTextSize(getTextSize() * 0.83f);
        tableHandler.setTextColor(textColor);
        registerHandler("table", tableHandler);

    }

    @Override
    public Spannable fromHtml(String html) {
        return super.fromHtml(html
                .replaceAll("((\r)?\n)+", "")
                .replaceAll(" +", " ")
                .replaceAll("&nbsp;", " ")
        );
    }
}
