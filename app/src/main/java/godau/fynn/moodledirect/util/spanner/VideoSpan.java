package godau.fynn.moodledirect.util.spanner;

/**
 * Contains a relative path and is replaced by {@link godau.fynn.moodledirect.view.ImageLoaderTextView}.
 */
public class VideoSpan {

    public final String source;

    public VideoSpan(String source) {
        this.source = source;
    }
}
