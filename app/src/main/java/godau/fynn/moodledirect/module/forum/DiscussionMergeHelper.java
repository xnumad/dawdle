package godau.fynn.moodledirect.module.forum;

/**
 * POJO that helps with merging Discussion objects in {@link Forum} module.
 */
public class DiscussionMergeHelper {
    public int id, forumId, lastPostUserId, replyCount;
    public boolean pinned;
}
