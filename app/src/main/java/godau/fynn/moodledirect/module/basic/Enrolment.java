package godau.fynn.moodledirect.module.basic;

import androidx.annotation.Nullable;
import godau.fynn.moodledirect.model.api.enrol.EnrolmentOption;
import godau.fynn.moodledirect.model.api.enrol.GuestEnrolOption;
import godau.fynn.moodledirect.model.api.enrol.SelfEnrolmentOption;
import godau.fynn.moodledirect.module.ModuleD;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Responsible for API calls starting with <code>enrol_</code> and <code>core_enrol_</code>,
 * except for <code>core_enrol_get_users_courses</code> for the signed-in user, which is
 * handled in {@link Core}.
 */
public class Enrolment extends ModuleD {

    public List<EnrolmentOption> getEnrolmentMethods(int courseId) throws IOException {
        List<EnrolmentOption> options = moodleServices.getEnrolmentMethods(
                userAccount.getToken(), courseId
        ).execute().body();

        List<EnrolmentOption> augmentedOptions = new ArrayList<>();

        for (EnrolmentOption option : options) {
            if ("enrol_guest_get_instance_info".equals(option.wsfunction)) {
                augmentedOptions.add(getGuestEnrolmentInformation(option.id));
            } else if ("enrol_self_get_instance_info".equals(option.wsfunction)) {
                augmentedOptions.add(getSelfEnrolmentInformation(option.id));
            } else augmentedOptions.add(option);
        }

        return augmentedOptions;
    }

    private SelfEnrolmentOption getSelfEnrolmentInformation(int instance) throws IOException {
        return moodleServices.getSelfEnrolmentInstanceInformation(
                userAccount.getToken(), instance
        ).execute().body();
    }

    private GuestEnrolOption getGuestEnrolmentInformation(int instance) throws IOException {
        return moodleServices.getGuestEnrolmentInstanceInformation(
                userAccount.getToken(), instance
        ).execute().body().instanceinfo;
    }

    public boolean selfEnrol(int courseId, int instanceId) throws IOException {
        return selfEnrol(courseId, instanceId, null);
    }

    public boolean selfEnrol(int courseId, int instanceId, @Nullable String password) throws IOException {
        return moodleServices.selfEnrolUserInCourse(
                userAccount.getToken(), courseId, instanceId, password
        ).execute().body().status;

    }
}
