package godau.fynn.moodledirect.module.link;

import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.fragment.module.forum.ForumFragment;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.Module;

import static godau.fynn.moodledirect.util.Constants.API_URL;

public class ForumLink extends ModuleLink {
    @Override
    protected void onOpen(Module module, Course course, FragmentActivity context, View drawContext) {
        Fragment forumFragment = ForumFragment.newInstance(module.getInstance(), course.shortname);
        showFragment(context, forumFragment, "Forum");
    }

    @Override
    protected String[] requiresCalls() {
        // From Moodle 3.7
        return new String[]{
                "mod_forum_get_forum_discussions",
                "mod_forum_get_discussion_posts"
        };
    }

    @Override
    public int getIcon() {
        return R.drawable.forum;
    }

    @Override
    public int getName() {
        return R.string.supported_modules_forum;
    }

    @Override
    public int getRequirementText() {
        return R.string.supported_modules_requirement_moodle_37;
    }

    public static String getForumUrl(int forumId) {
        return API_URL + "mod/forum/view.php?id=" + forumId;
    }

    public static String getDiscussionUrl(int discussionId) {
        return API_URL + "mod/forum/discuss.php?d=" + discussionId;
    }
}
