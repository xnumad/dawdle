package godau.fynn.moodledirect.module.link;

import android.view.View;

import androidx.fragment.app.FragmentActivity;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.MyApplication;

public class ZoomLink extends ModuleLink {
    @Override
    protected void onOpen(Module module, Course course, FragmentActivity context, View drawContext) {
        Snackbar zoomSnackbar = Snackbar.make(
                drawContext,
                R.string.zoom_progress_downloading_link,
                BaseTransientBottomBar.LENGTH_INDEFINITE);
        zoomSnackbar.show();
        ExceptionHandler.tryAndThenThread(
                () -> MyApplication.moodle().getDispatch().getZoom().getConferenceLink(module.id),
                link -> {
                    ModuleLink.openWebsite(context, link); // No automagic login for external URL
                    zoomSnackbar.dismiss();
                },
                otherwise -> zoomSnackbar.dismiss(),
                context
        );
    }

    @Override
    protected String[] requiresCalls() {
        return new String[]{"mod_zoom_grade_item_update"};
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_conference;
    }

    @Override
    public int getName() {
        return R.string.zoom;
    }

    @Override
    public int getRequirementText() {
        return R.string.supported_modules_requirement_zoom;
    }
}
