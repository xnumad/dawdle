package godau.fynn.moodledirect.module.basic;

import android.text.Html;
import android.util.Log;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import godau.fynn.moodledirect.model.api.base.Category;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.network.MoodleServices;
import godau.fynn.moodledirect.network.Validator;

public class CategoryResolver {

    private final MoodleServices moodleServices;
    private final String token;

    public CategoryResolver(MoodleServices moodleServices, String token) {
        this.moodleServices = moodleServices;
        this.token = token;
    }

    /**
     * Adds top- and bottommost category to provided {@link Course} objects
     * @return Map from course IDs to their labels
     */
    public Map<Integer, Category> resolve(List<Course> courseList) throws IOException {

        Map<Integer, Category> categoryMap = new HashMap<>();

        // 1st step: query for bottom-level category ids
        StringBuilder queryIds = new StringBuilder();

        for (Course course : courseList) {
            queryIds.append(course.categoryId).append(",");
        }
        if (queryIds.length() > 0)
            queryIds.delete(queryIds.length()-1, queryIds.length()); //remove trailing ','

        Category[] categories = moodleServices.getCourseCategories(token, queryIds.toString()).execute().body();

        // 2nd step: query for top-level category ids while map-ifying categories

        queryIds = new StringBuilder();

        for (Category category : categories) {
            // Map-ify returned categories
            categoryMap.put(category.id, category);

            String nextId = category.path.substring(1).split("/")[0];

            if (categoryMap.containsKey(nextId)) continue;

            // Add top-level categories
            queryIds.append(nextId).append(",");
        }
        if (queryIds.length() > 0)
            queryIds.delete(queryIds.length()-1, queryIds.length()); //remove trailing ','

        if (queryIds.length() > 0) {

            categories = moodleServices.getCourseCategories(token, queryIds.toString()).execute().body();

            // 3rd step: map-ify returned categories

            for (Category category : categories) {
                categoryMap.put(category.id, category);
            }
        }

        // 4th step: assign to Courses
        for (Course course : courseList) {
            Category category = categoryMap.get(course.categoryId);
            if (category != null) {
                course.categoryName = Html.fromHtml(category.name).toString();

                int topCategoryId = Integer.parseInt(category.path.substring(1).split("/")[0]);
                Category topCategory = categoryMap.get(topCategoryId);
                if (topCategory != null) {
                    course.topCategoryName = Html.fromHtml(
                            topCategory.name
                    ).toString();
                } else {
                    course.topCategoryName = null;
                }
            } else {
                course.categoryName = course.topCategoryName = null;
            }
        }

        return categoryMap;
    }
}
