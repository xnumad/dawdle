package godau.fynn.moodledirect.module.basic;

import android.util.Log;
import androidx.room.*;
import com.google.gson.JsonSyntaxException;
import godau.fynn.moodledirect.OfflineException;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.api.MoodleException;
import godau.fynn.moodledirect.model.api.SiteInformation;
import godau.fynn.moodledirect.model.api.base.CourseList;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.module.ModuleD;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * DAO for core moodle functionality. Scope is roughly equivalent to the {@code core_course}
 * API area excluding course content and search.
 */
public abstract class Core extends ModuleD {

    public abstract List<Course> getCourses() throws IOException;

    public abstract Course getCourse(int id) throws IOException;

    public abstract SiteInformation getSiteInformation() throws IOException;

    public abstract List<Course> searchCourses(String query) throws IOException;

    public boolean isEnrolledTo(int courseId) throws IOException {
        return getCourses().stream().anyMatch(course -> course.id == courseId);
    }

    @Dao
    public static abstract class Offline extends Core {

        @Transaction
        @Query("SELECT * FROM course ORDER BY id DESC")
        public abstract List<Course> getCoursesFromDatabase();

        @Query("SELECT id FROM course WHERE id = :courseId")
        public abstract Integer hasCourseInDatabase(int courseId);

        @Override
        public List<Course> getCourses() throws IOException {
            List<Course> courses = getCoursesFromDatabase();
            if (getCoursesFromDatabase().isEmpty()) throw new OfflineException();
            else return courses;
        }

        @Transaction
        @Query("SELECT * FROM course WHERE id = :id")
        public abstract Course getCourse(int id);

        @Override
        public SiteInformation getSiteInformation() {
            throw new OfflineException();
        }

        @Override
        public List<Course> searchCourses(String query) {
            throw new OfflineException(R.string.exception_offline_search);
        }

        @Override
        public boolean isEnrolledTo(int courseId) {
            return hasCourseInDatabase(courseId) != null;
        }
    }

    @Dao
    public static abstract class Online extends Core {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        protected abstract void insertCourses(List<Course> courseList);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        protected abstract void insertCourse(Course course);

        @Query("DELETE FROM course")
        protected abstract void deleteCourses();

        @Transaction
        protected void setCourses(List<Course> courseList) {
            deleteCourses();
            insertCourses(courseList);
        }

        /**
         * Downloads course data and stores it into the database.
         * @throws MoodleException In case the Moodle server returns an error
         * @return Latest course data
         */
        @Override
        public List<Course> getCourses() throws IOException, MoodleException, JsonSyntaxException {
            Log.d("userid", userAccount.getUserId() + "");
            List<Course> courseList = moodleServices.getCourses(userAccount.getToken(), userAccount.getUserId()).execute().body();

            for (Course course : courseList) {
                course.prepareAfterDownload();
            }

            if (courseList.size() > 0) {
                CategoryResolver categoryResolver = new CategoryResolver(moodleServices, userAccount.getToken());
                categoryResolver.resolve(courseList);
            }

            setCourses(courseList);

            Collections.sort(courseList, Comparator.comparing(o -> Integer.MAX_VALUE - o.id));
            return courseList;
        }

        @Override
        public Course getCourse(int id) throws IOException {
            Course course = moodleServices.getCourseById(userAccount.getToken(), id).execute().body()
                            .courses.get(0);

            course.prepareAfterDownload();

            CategoryResolver categoryResolver = new CategoryResolver(moodleServices, userAccount.getToken());
            categoryResolver.resolve(Collections.singletonList(course));

            insertCourse(course);

            return course;
        }

        @Override
        public SiteInformation getSiteInformation() throws IOException {
            return moodleServices.getSiteInformation(userAccount.getToken()).execute().body();
        }

        @Override
        public List<Course> searchCourses(String query) throws IOException {
            CourseList result = moodleServices.searchCourses(
                    userAccount.getToken(),
                    query
            ).execute().body();

            for (Course course : result.courses) {
                course.prepareAfterDownload();
            }

            return result.courses;
        }
    }
}
