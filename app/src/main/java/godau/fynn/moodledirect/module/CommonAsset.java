package godau.fynn.moodledirect.module;

import android.util.Log;
import androidx.annotation.Nullable;
import androidx.room.Dao;
import godau.fynn.moodledirect.activity.fragment.module.PageFragment;
import godau.fynn.moodledirect.module.files.AssetLoader;

import java.io.File;
import java.io.IOException;

/**
 * Downloads inline images.
 */
@Dao
public abstract class CommonAsset extends ModuleD implements AssetLoader {

    /**
     * @param sendAuth Should be false for external sources in order not to leak authentication details
     */
    @Override
    @Nullable
    public File getAsset(String url, boolean sendAuth) throws IOException {

        // Try to load from cache

        File storageLocation = getFileLocation(url);

        if (storageLocation.isFile()) {
            Log.d(PageFragment.class.getSimpleName(), "Loading file " + url +
                    " from cache");
            return storageLocation;
        } else return null;
    }

    @Dao
    public static abstract class Online extends CommonAsset {
        /**
         * @param sendAuth Should be false for external sources in order not to leak the token
         */
        @Nullable
        public File getAsset(String url, boolean sendAuth) throws IOException {

            // Prefer offline storage or cache
            File cacheFile = super.getAsset(url, sendAuth);
            if (cacheFile != null) return cacheFile;


            // Download file from URL
            return download(url, sendAuth);
        }
    }

}
