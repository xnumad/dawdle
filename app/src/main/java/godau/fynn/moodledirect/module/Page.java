package godau.fynn.moodledirect.module;

import android.text.SpannableStringBuilder;
import androidx.room.Dao;
import godau.fynn.moodledirect.OfflineException;
import godau.fynn.moodledirect.util.TextUtil;
import okio.Okio;

import java.io.File;
import java.io.IOException;

@Dao
public abstract class Page extends ModuleD {

    public SpannableStringBuilder getPage(String url) throws IOException {
        File file = database.getCommonAsset().getAsset(url, true);
        if (file == null) {
            throw new OfflineException();
        }
        String responseString = Okio.buffer(Okio.source(file)).readUtf8();
        return TextUtil.fromHtml(responseString, context);
    }

    @Dao
    public static abstract class Online extends Page {

        @Override
        public SpannableStringBuilder getPage(String url) throws IOException {
            // Refresh cache
            download(url, true);
            return super.getPage(url);
        }
    }
}
