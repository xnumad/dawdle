package godau.fynn.moodledirect.view.adapter.forum;

import android.text.format.DateUtils;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import godau.fynn.moodledirect.model.api.forum.Discussion;
import godau.fynn.moodledirect.network.NetworkStateReceiver;
import godau.fynn.typedrecyclerview.SimpleRecyclerViewAdapter;

import java.util.List;

public abstract class CommonForumAdapter<VH extends ForumViewHolder> extends SimpleRecyclerViewAdapter<Discussion, VH> {
    public CommonForumAdapter(List<Discussion> content) {
        super(content);
    }

    public void addDiscussions(List<Discussion> discussions) {
        content.addAll(discussions);
        notifyDataSetChanged();
    }

    public void clearDiscussions() {
        content.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ForumViewHolder holder, Discussion discussion, int position) {
        // Must not collide with non-matching views in target fragment
        holder.itemView.setTransitionName(discussion.forumId + "card" + position);

        // Load avatar, don't attempt to use network if offline
        RequestCreator creator = Picasso.get().load(discussion.userAvatar);
        if (NetworkStateReceiver.getOfflineStatus()) {
            creator.networkPolicy(NetworkPolicy.OFFLINE);
        }
        creator.into(holder.avatar);

        holder.subject.setText(discussion.subject);
        holder.userName.setText(discussion.userName);
        holder.modifiedTime.setText(DateUtils.getRelativeTimeSpanString(
                context, discussion.created * 1000L
        ));
    }
}
