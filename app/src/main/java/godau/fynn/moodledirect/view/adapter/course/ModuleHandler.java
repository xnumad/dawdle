package godau.fynn.moodledirect.view.adapter.course;

import android.content.res.Resources;
import android.net.Uri;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionManager;
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou;
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYouListener;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.ResourceType;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.module.link.ModuleLink;
import godau.fynn.moodledirect.network.NetworkStateReceiver;
import godau.fynn.moodledirect.util.FileManagerWrapper;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.util.TextUtil;
import godau.fynn.moodledirect.view.DownloadItemViewHolder;
import godau.fynn.moodledirect.view.ExpandableTextDisplay;
import godau.fynn.moodledirect.view.ImageLoaderTextView;
import godau.fynn.typedrecyclerview.TypeHandler;

public class ModuleHandler extends TypeHandler<ModuleHandler.ModuleViewHolder, Module> {
    private final FileManagerWrapper fileManager;
    private final ModuleClickListener clickListener;
    private Course course;

    public ModuleHandler(FileManagerWrapper fileManager) {
        this.fileManager = fileManager;

        // For performance, we only use one instance of our custom click listener class
        clickListener = new ModuleClickListener();
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public ModuleViewHolder createViewHolder(@NonNull ViewGroup parent) {
        return new ModuleViewHolder(
                (ViewGroup) inflater.inflate(R.layout.row_course_module_resource, parent, false)
        );
    }

    @Override
    public void bindViewHolder(@NonNull ModuleViewHolder holder, Module item, int position) {

        Log.d(ModuleHandler.class.getSimpleName(),
                "Binding to module \"" + item.getName() + "\" of type " + item.getModuleType() + " / " + item.moduleType + "."
        );

        holder.name.setText(item.getName());

        if (item.getDescription() != null && !item.getDescription().isEmpty()) {

            ExpandableTextDisplay expandableTextDisplay = item.getExpandableTextDisplay(context, recyclerViews.get(0).getWidth());
            TextUtil.setTextExpandable(holder.description, expandableTextDisplay, position, recyclerViews.get(0), context);

            holder.description.setMovementMethod(LinkMovementMethod.getInstance());
            holder.description.setVisibility(View.VISIBLE);
        } else {
            holder.description.setVisibility(View.GONE);
        }

        holder.displayDownloadStatus(item, false);

        int resourceIcon = item.getModuleIcon() + ModuleLink.getIcon(item);
        if (resourceIcon != Resources.ID_NULL) {
            holder.icon.setImageResource(resourceIcon);
            holder.icon.setVisibility(View.VISIBLE);
            holder.icon.setScaleX(1f);
            holder.icon.setScaleY(1f);
            holder.colorBackground.setVisibility(View.GONE);
        } else if (NetworkStateReceiver.getOfflineStatus()) {
            holder.icon.setVisibility(View.VISIBLE);
            holder.icon.setImageDrawable(context.getDrawable(R.drawable.ic_file_generic));
            holder.icon.setScaleX(1f);
            holder.icon.setScaleY(1f);
            holder.colorBackground.setVisibility(View.GONE);
        } else {
            holder.icon.setVisibility(View.INVISIBLE);
            holder.progressBar.animate().alpha(1f);
            GlideToVectorYou
                    .init()
                    .with(context)
                    .setPlaceHolder(0, R.drawable.ic_file_generic)
                    .withListener(new GlideToVectorYouListener() {
                        @Override
                        public void onLoadFailed() {
                            Log.w("Module image", "Couldn't download module icon from " + item.moduleIconUrl);
                            holder.progressBar.animate().alpha(0f);
                            holder.icon.setVisibility(View.VISIBLE);
                            holder.displayDownloadStatus(item, true);
                        }

                        @Override
                        public void onResourceReady() {
                            holder.progressBar.animate().alpha(0f);
                            holder.icon.setVisibility(View.VISIBLE);
                            if (MyApplication.getInstance().isDarkModeEnabled()) {
                                holder.colorBackground.setVisibility(View.VISIBLE);
                                holder.icon.setScaleX(0.7f);
                                holder.icon.setScaleY(0.7f);
                            } else {
                                holder.icon.setScaleX(1f);
                                holder.icon.setScaleY(1f);
                                holder.colorBackground.setVisibility(View.GONE);
                            }
                        }
                    })
                    .load(Uri.parse(item.moduleIconUrl), holder.icon);
        }

        // Disable item if not available
        holder.itemView.setEnabled(item.available);
        holder.itemView.setClickable(item.available);
        // Gray item out
        holder.moduleLayout.setAlpha(item.available? 1f : 0.5f);

        holder.itemView.setOnClickListener(clickListener);
        holder.itemView.setTag(ModuleClickListener.TAG_HOLDER, holder);
        holder.itemView.setTag(ModuleClickListener.TAG_ITEM, item);
    }

    private class ModuleClickListener implements View.OnClickListener {

        // Need to be resource ids
        public static final int TAG_HOLDER = R.id.frame;
        public static final int TAG_ITEM = R.id.content_main;

        @Override
        public void onClick(View v) {

            Module item = (Module) v.getTag(TAG_ITEM);
            ModuleViewHolder holder = (ModuleViewHolder) v.getTag(TAG_HOLDER);

            if (item.isDownloadable() && item.getModuleType() == ResourceType.FILE) {
                switch (item.contents.get(0).downloadStatus) {
                    case NOT_DOWNLOADED:
                    case FAILED:
                    case UPDATE_AVAILABLE: // TODO: delete old file
                        if (fileManager.startDownload(item.contents.get(0), course.shortname, context, uri -> {

                            if (item.contents.get(0).equals(holder.downloadIcon.getTag())) {

                                // Refresh download status of all visible views
                                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerViews.get(0).getLayoutManager();

                                for (int i = layoutManager.findFirstVisibleItemPosition();
                                     i <= layoutManager.findLastVisibleItemPosition(); i++
                                ) {
                                    RecyclerView.ViewHolder childHolder = recyclerViews.get(0).getChildViewHolder(
                                            layoutManager.findViewByPosition(i)
                                    );

                                    if (childHolder instanceof ModuleViewHolder) {
                                        ((ModuleViewHolder) childHolder).displayDownloadStatus(
                                                ((Module) content.get(i)), true
                                        );
                                    }
                                }

                            } else {
                                Log.d(ModuleHandler.class.getSimpleName(), "Item was reassigned, not displaying new status");
                            }

                        })) {
                            TransitionManager.beginDelayedTransition(holder.itemView);
                            holder.downloadIcon.setVisibility(View.GONE);
                            holder.progressBar.animate().alpha(1f);
                        }
                        break;
                    case DOWNLOADING:
                        // TODO Show download status
                        break;
                    case DOWNLOADED:
                        fileManager.openFile(item.getFileList().get(0), () -> holder.displayDownloadStatus(item, true), context);
                        break;
                }
            } else {
                ModuleLink.open(item, course, (FragmentActivity) context, recyclerViews.get(0));
            }
        }
    }

    static class ModuleViewHolder extends DownloadItemViewHolder {

        final ImageLoaderTextView description;
        final ViewGroup moduleLayout;
        final ImageView colorBackground;

        public ModuleViewHolder(ViewGroup itemView) {
            super(itemView);

            description = itemView.findViewById(R.id.description);
            moduleLayout = itemView.findViewById(R.id.moduleLayout);
            colorBackground = itemView.findViewById(R.id.colorBackground);

            description.setMovementMethod(LinkMovementMethod.getInstance());
            description.setLinksClickable(true);
        }
    }
}
