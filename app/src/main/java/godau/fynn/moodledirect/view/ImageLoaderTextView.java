package godau.fynn.moodledirect.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ImageSpan;
import android.text.style.URLSpan;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;

import com.sysdata.htmlspanner.spans.HorizontalLineSpan;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.module.files.AssetLoader;
import godau.fynn.moodledirect.util.Constants;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.Util;
import godau.fynn.moodledirect.util.spanner.EnhancedImageSpan;
import godau.fynn.moodledirect.util.spanner.VideoSpan;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * This kind of {@link android.widget.TextView} is capable of downloading inline embedded images.
 * Its width should <b>always be set to {@code MATCH_PARENT}</b>, as images will scale to be no larger than
 * the parent view horizontally.
 */
public class ImageLoaderTextView extends androidx.appcompat.widget.AppCompatTextView {

    private SpannableStringBuilder spannable;
    private final List<Thread> threads = new LinkedList<>();

    private List<String> urlList;
    private AssetLoader loader;
    private DisplayMetrics metrics;

    private boolean fillImagesAfterMeasure;

    public ImageLoaderTextView(@NonNull Context context) {
        super(context);
    }

    public ImageLoaderTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageLoaderTextView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setText(SpannableStringBuilder spannable, @NonNull List<String> urlList, AssetLoader loader, DisplayMetrics displayMetrics) {

        this.spannable = spannable;

        for (Thread thread : threads) {
            thread.interrupt();
        }

        synchronized (this.spannable) {
            setText(spannable);
        }


        this.urlList = urlList;
        this.loader = loader;
        this.metrics = displayMetrics;

        // Don't try to load images if view had not been measured yet (we get 0x0 images otherwise)
        if (getWidth() > 0) {
            metrics.widthPixels = getWidth();
            metrics.widthPixels -= getTotalPaddingLeft() + getTotalPaddingRight();
            fillImages();
        } else fillImagesAfterMeasure = true;

        setLinksClickable(true);
        setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (fillImagesAfterMeasure) {
            fillImagesAfterMeasure = false;
            post(() -> {
                metrics.widthPixels = getWidth();
                metrics.widthPixels -= getTotalPaddingLeft() + getTotalPaddingRight();
                fillImages();
            });
        }
    }

    /**
     * @see <a href="https://gist.github.com/anonymous/1190397">Original gist by an unknown author</a>
     */
    private void fillImages() {

        String privateAccessKey = new PreferenceHelper(getContext()).getUserAccount().getFilePrivateAccessKey();

        // Download each imageSpan to cache if not exists
        for (ImageSpan placeholderSpan : spannable.getSpans(0, spannable.length(), ImageSpan.class)) {

            threads.add(ExceptionHandler.tryAndThenThread(
                    () -> {

                        // Do not leak auth to external servers
                        boolean external = true;
                        String source = placeholderSpan.getSource();
                        if (source != null) for (String url : urlList) {
                            // Expand to any matching complete file path
                            if (url.contains(placeholderSpan.getSource())) {
                                source = url;
                                external = false;
                                break;
                            }
                        } else {
                            // Do not attempt to redraw this asset
                            return spannable;
                        }

                        // Recheck this
                        if (external) {
                            /* Login procedure should guarantee us that there is a trailing '/' character at the end of
                             * the "API URL", hindering abuse. Images that require moodle auth should reside only in
                             * subpaths of the API URL.
                             *
                             * Images that are provided by the moodle file provider and not attached to a specific
                             * module need special treatment: the token needs to be added to the URL.
                             */
                            if (source.startsWith(Constants.API_URL)) {
                                // We probably don't need to send the token as a query parameter in this case
                                external = false;

                                source = source
                                        /* This case happens only sometimes, for instance when viewing inline attached
                                         * photos in forum posts, but not when viewing inline photos in labels.
                                         */
                                        .replace("/webservice/pluginfile.php/", "/pluginfile.php/")
                                        .replace("/pluginfile.php/",
                                                "/tokenpluginfile.php/" +
                                                        privateAccessKey + "/"
                                        );
                            }
                        }

                        // Load image asset
                        Drawable drawable;
                        try {
                            File imageFile = loader.getAsset(source, !external);

                            if (imageFile != null) {
                                drawable = new BitmapDrawable(getResources(),
                                        imageFile.getAbsolutePath());

                                // do some scaling
                                int width, height;
                                int originalWidthScaled = (int) (drawable.getIntrinsicWidth() * metrics.density);
                                int originalHeightScaled = (int) (drawable.getIntrinsicHeight() * metrics.density);

                                if (placeholderSpan instanceof EnhancedImageSpan) {
                                    originalWidthScaled = ((EnhancedImageSpan) placeholderSpan).getWidth();
                                    originalHeightScaled = ((EnhancedImageSpan) placeholderSpan).getHeight();
                                }

                                if (originalWidthScaled > metrics.widthPixels) {
                                    height = drawable.getIntrinsicHeight() * metrics.widthPixels
                                            / drawable.getIntrinsicWidth();
                                    width = metrics.widthPixels;
                                } else {
                                    height = originalHeightScaled;
                                    width = originalWidthScaled;
                                }

                                // it's important to call setBounds otherwise the image will
                                // have a size of 0px * 0px and won't show at all
                                drawable.setBounds(0, 0, width, height);

                            } else {
                                drawable = AppCompatResources.getDrawable(getContext(), R.drawable.ic_cloud_off);
                                int size = Util.spToPx(16, getContext());
                                drawable.setBounds(0, 0, size, size);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();

                            drawable = AppCompatResources.getDrawable(getContext(), R.drawable.ic_error);
                            int size = Util.spToPx(16, getContext());
                            drawable.setBounds(0, 0, size, size);
                        }

                        // now we create a new ImageSpan
                        ImageSpan newImageSpan = new ImageSpan(drawable, placeholderSpan.getSource());

                        synchronized (spannable) {
                            replaceImageSpanAndSetTextView(spannable, placeholderSpan, newImageSpan);
                        }

                        return spannable;
                    },
                    // Nothing left to do
                    updatedSpannable -> {
                        synchronized (spannable) {
                            setText(spannable);
                        }
                    },
                    getContext()
            ));
        }

        // Replace each video span with absolute path if possible (synchronously without network action)
        for (VideoSpan videoSpan : spannable.getSpans(0, spannable.length(), VideoSpan.class)) {
            String source = videoSpan.source;
            if (source != null) {
                boolean match = false;
                for (String url : urlList) {
                    // Expand to any matching complete file path
                    if (url.contains(videoSpan.source)) {
                        source = url;
                        match = true;
                        break;
                    }
                }

                if (!match) {
                    // Try again without query parameters
                    source = videoSpan.source.replaceAll("\\?.*", "");
                    for (String url : urlList) {
                        if (url.contains(source)) {
                            source = url;
                            match = true;
                            break;
                        }
                    }
                }

                // A match should have been found here, otherwise no URL is available
                if (!match) {
                    Log.e(ImageLoaderTextView.class.getSimpleName(), "Found no match for " + videoSpan.source);
                    continue;
                }

                // Note that external links are already converted accordingly in VideoHandler
                source = source
                        .replace("/webservice/pluginfile.php/", "/pluginfile.php/")
                        .replace("/pluginfile.php/",
                                "/tokenpluginfile.php/" +
                                        privateAccessKey + "/"
                        ); // like above

                spannable.setSpan(new URLSpan(source),
                        spannable.getSpanStart(videoSpan),
                        spannable.getSpanEnd(videoSpan),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                );
                spannable.removeSpan(videoSpan);
            }
        }
    }

    private void replaceImageSpanAndSetTextView(SpannableStringBuilder spannable, ImageSpan old, ImageSpan replacement) {

        // find the position of the old ImageSpan
        int start = spannable.getSpanStart(old);
        int end = spannable.getSpanEnd(old);

        // remove the old ImageSpan
        spannable.removeSpan(old);

        // add the new ImageSpan
        if (start != -1 & end != -1) spannable.setSpan(replacement, start, end, Spannable.SPAN_POINT_MARK);

    }

}
