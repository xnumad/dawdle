package godau.fynn.moodledirect.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.TextViewCompat;
import godau.fynn.moodledirect.OfflineException;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.util.ExceptionHandler;

import java.io.IOException;

/**
 * Indicates that no data is available, either because of a network error,
 * because an empty list was loaded or because offline mode is enabled and
 * no data is available in storage.
 */
public class NoDataView extends AppCompatTextView {

    private InvokeListener invokeListener = () -> {};

    public NoDataView(@NonNull Context context) {
        this(context, null);
    }

    public NoDataView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, android.R.attr.textViewStyle);
    }

    public NoDataView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setGravity(Gravity.CENTER);
        setText(R.string.offline_not_available);

        // Get textColorPrimary attribute
        TypedValue value = new TypedValue();
        getContext().getTheme().resolveAttribute(android.R.attr.textColorPrimary, value, true);
        setTextColor(getContext().getResources().getColor(value.resourceId));

        // Get colorControlNormal attribute
        getContext().getTheme().resolveAttribute(android.R.attr.colorControlNormal, value, true);
        TextViewCompat.setCompoundDrawableTintList(this, ColorStateList.valueOf(
                getContext().getResources().getColor(value.resourceId)
        ));

        setVisibility(GONE);
    }

    public void text(@StringRes int text) {
        setCompoundDrawables(null, null, null, null);
        setText(text);
        setVisibility(VISIBLE);
        invokeListener.onInvokeNoData();
    }

    public void text(@StringRes int text, @DrawableRes int drawable) {
        setCompoundDrawablesRelativeWithIntrinsicBounds(
                0, drawable, 0, 0
        );
        setText(text);
        setVisibility(VISIBLE);
        invokeListener.onInvokeNoData();
    }

    public void exception(Exception e) {
        if (e instanceof OfflineException) notAvailableOffline();
        else if (e instanceof IOException) networkError(((IOException) e));
        else {
            text(R.string.error_generic, R.drawable.ic_error);
        }
        invokeListener.onInvokeNoData();
    }

    public void notAvailableOffline() {
        text(R.string.offline_not_available, R.drawable.ic_cloud_off);
        invokeListener.onInvokeNoData();
    }

    public void networkError(IOException e) {
        text(
                ExceptionHandler.networkErrorMessage(e),
                R.drawable.ic_error
        );
    }

    public void hide() {
        setVisibility(GONE);
    }

    public void setOnInvokeListener(InvokeListener invokeListener) {
        this.invokeListener = invokeListener;
    }

    public interface InvokeListener {
        public void onInvokeNoData();
    }
}
