package godau.fynn.moodledirect;

import android.content.res.Resources;
import androidx.annotation.StringRes;

/**
 * Signifies that an action cannot be performed while offline
 */
public class OfflineException extends UnsupportedOperationException {

    private final @StringRes int cause;

    /**
     * {@link godau.fynn.moodledirect.util.ExceptionHandler} will show a popup if this constructor
     * is used.
     *
     * @param cause Message to be displayed to the user in error popup
     */
    public OfflineException(@StringRes int cause) {
        this.cause = cause;
    }

    public OfflineException() {
        cause = Resources.ID_NULL;
    }

    public int getCauseMessage() {
        return cause;
    }
}
