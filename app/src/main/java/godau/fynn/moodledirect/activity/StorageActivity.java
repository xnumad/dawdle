package godau.fynn.moodledirect.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.documentfile.provider.DocumentFile;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.MyApplication;

import java.io.File;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;

public class StorageActivity extends AppCompatActivity {

    public static final String EXTRA_UP_FINISHES = "upFinishes";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (MyApplication.getInstance().isDarkModeEnabled()) {
            setTheme(R.style.AppTheme_Dark);
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_storage);

        // Closing the database removes -shm and -wal files which represent pending database transactions.
        MyApplication.commitDatabase();

        showDatabaseSize();
        showCacheSize();
        showFileSize();

        // Show action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.settings_storage_manager);
        getSupportActionBar().show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (getIntent().getBooleanExtra(EXTRA_UP_FINISHES, false)) {
            finish();
            return true;
        } else return super.onSupportNavigateUp();
    }

    private void showDatabaseSize() {
        List<File> files = flattenDirectory(getDatabasePath(new PreferenceHelper(this).getUserAccount().getDatabaseName())
                .getParentFile());

        long bytes = 0;
        for (File file : files) {
            bytes += file.length();
        }

        double megaBytes = bytes / (1000d * 1000d);

        TextView storageDatabases = findViewById(R.id.storage_databases);
        storageDatabases.setText(getString(R.string.storage_databases, NumberFormat.getInstance().format(megaBytes)));

        Button clear = findViewById(R.id.clear_database);
        clear.setOnClickListener(v -> ExceptionHandler.tryAndThenThread(
                () -> {
                    MyApplication.moodle().getCleaner().clearDatabase(MyApplication.moodle());
                    MyApplication.commitDatabase();
                    return null;
                },
                then -> showDatabaseSize(),
                this
        ));
    }

    private void showCacheSize() {
        List<File> files = flattenDirectory(getCacheDir());

        long bytes = 0;
        for (File file : files) {
            bytes += file.length();
        }

        double megaBytes = bytes / (1000d * 1000d);

        TextView storageCache = findViewById(R.id.storage_cache);
        storageCache.setText(getString(R.string.storage_cache, NumberFormat.getInstance().format(megaBytes)));

        Button clear = findViewById(R.id.clear_cache);
        clear.setOnClickListener(v -> {
            for (File file : files) {
                Log.d(StorageActivity.class.getSimpleName(),
                        "Deleting " + file.getName() + " (success = " + file.delete() + ")");
            }
            showCacheSize();
        });
    }

    private void showFileSize() {

        ExceptionHandler.tryAndThenThread(() -> MyApplication.moodle().getDispatch().getFile()
                        .getAllFiles(),
                downloadedFiles -> {

                    long bytes = downloadedFiles.stream()
                            .map(downloadedFile -> DocumentFile.fromSingleUri(this, downloadedFile.location))
                            .mapToLong(file -> file.length())
                            .sum();

                    double megaBytes = bytes / (1000d * 1000d);

                    TextView storageFile = findViewById(R.id.storage_files);
                    storageFile.setText(getString(R.string.storage_files, NumberFormat.getInstance().format(megaBytes)));

                    Button clear = findViewById(R.id.clear_files);
                    clear.setOnClickListener(v -> {
                        long deleteSuccess = downloadedFiles
                                .stream()
                                .map(downloadedFile -> DocumentFile.fromSingleUri(this, downloadedFile.location))
                                .peek(file -> Log.d(StorageActivity.class.getSimpleName(), "Deleting " + file.getName()))
                                .filter(DocumentFile::delete)
                                .count();
                        Log.d(StorageActivity.class.getSimpleName(), "Success: " + deleteSuccess);

                        ExceptionHandler.tryAndThenThread(
                                () -> MyApplication.moodle().getDispatch().getFile().clear(),
                                success -> {},
                                this
                        );

                        showFileSize();
                    });
                }, this);
    }

    private List<File> flattenDirectory(File directory) {
        List<File> files = new LinkedList<>();

        if (!directory.isDirectory()) {
            files.add(directory);
        } else for (File file : directory.listFiles()) {
            files.addAll(flattenDirectory(file));
        }
        return files;
    }
}
