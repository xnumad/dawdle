package godau.fynn.moodledirect.activity.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.network.NetworkStateReceiver;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.view.NoDataView;

/**
 * Fragment that provides two features for descendant classes:
 * <ul>
 *     <li><code>SwipeRefreshLayout</code> container</li>
 *     <li>{@link NoDataView} in the middle</li>
 * </ul>
 */
public abstract class SwipeRefreshFragment extends Fragment {

    protected SwipeRefreshLayout swipeRefreshLayout;
    protected NoDataView empty;

    // Variables are kept when restored from back stack
    private boolean loaded = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FrameLayout frameLayout = (FrameLayout) inflater.inflate(R.layout.fragment_swipe_refresh, container, false);
        // Do this while we are certain that the IDs are unique (also we need to inflate into the swipeRefreshLayout)
        swipeRefreshLayout = frameLayout.findViewById(R.id.swipeRefreshLayout);
        empty = frameLayout.findViewById(R.id.empty);

        swipeRefreshLayout.addView(onCreateContentView(inflater, container, savedInstanceState));
        return frameLayout;
    }

    protected abstract View onCreateContentView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

    @Override
    @CallSuper
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        swipeRefreshLayout.setOnRefreshListener(() -> loadData(MyApplication.moodle().getDispatch()));

        // In offline mode, queries are so quick that the indicator would flicker. Don't show it in the first place.
        if (!loaded && !NetworkStateReceiver.getOfflineStatus())
            swipeRefreshLayout.setRefreshing(true);

        if (!loaded & savedInstanceState == null) {
            loadData(MyApplication.moodle().getDispatch());
            loaded = true;
        } else if (!loaded) {
            // Force offline dispatch if restoring instance state (avoid lag when navigating back)
            loadData(MyApplication.moodle().forceOffline());
            loaded = true;
        }

        Activity activity = requireActivity();
        if (activity instanceof NoDataView.InvokeListener) {
            empty.setOnInvokeListener((NoDataView.InvokeListener) activity);
        }
    }

    /**
     * Called after the fragment is instantiated or if pull-to-refresh was triggered.
     * At this moment, the <code>SwipeRefreshLayout</code> is already set to refreshing.
     *
     * @param dispatch Dispatch that should be used to download data
     */
    protected abstract void loadData(MoodleDatabase.Dispatch dispatch);
}
