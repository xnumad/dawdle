package godau.fynn.moodledirect.activity.fragment;

import android.app.AlertDialog;
import android.content.*;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.data.persistence.UserAccount;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.MyApplication;

public class CalendarFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayout icsAppBox = view.findViewById(R.id.ics_app_layout);
        TextView icsAppText = view.findViewById(R.id.ics_app_text);
        ImageView icsAppIcon = view.findViewById(R.id.ics_app_icon);
        Button export = view.findViewById(R.id.button);

        RadioButton scopeAll, scopeCourses, scopePersonal;
        scopeAll = view.findViewById(R.id.export_scope_all);
        scopeCourses = view.findViewById(R.id.export_scope_courses);
        scopePersonal = view.findViewById(R.id.export_scope_personal);

        Intent testIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("webcals://example.com/webcal.ics"));
        boolean appInstalled = requireContext().getPackageManager().queryIntentActivities(testIntent, 0).size() > 0;
        boolean recommendedAppInstalled =
                requireContext().getPackageManager().getLaunchIntentForPackage("at.bitfire.icsdroid") != null;

        if (recommendedAppInstalled) {
            icsAppText.setText(R.string.calendar_ics_app_recommended_installed);
            icsAppBox.setElevation(0f);
            icsAppBox.setClickable(false);
        } else if (appInstalled) {
            icsAppText.setText(R.string.calendar_ics_app_installed);
            icsAppBox.setElevation(0f);
            icsAppBox.setClickable(false);
            icsAppIcon.setVisibility(View.GONE);
        } else {
            icsAppBox.setOnClickListener(v -> {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.calendar_ics_app_recommended_link))));
                } catch (ActivityNotFoundException e) {
                    new AlertDialog.Builder(requireContext())
                            .setMessage(R.string.login_error_no_browser_installed)
                            .show();
                }
            });
        }

        export.setOnClickListener(v -> {

            // Determine which scope is selected
            String scope;
            if (scopeAll.isChecked()) scope = "all";
            else if (scopeCourses.isChecked()) scope = "courses";
            else if (scopePersonal.isChecked()) scope = "user";
            else throw new IllegalStateException();

            Context context = requireContext();
            export.setEnabled(false);
            ExceptionHandler.tryAndThenThread(
                    () -> MyApplication.moodle().getDispatch().getCalendar().getExportToken(),
                    token -> {

                        export.setEnabled(true);

                        UserAccount account = new PreferenceHelper(context).getUserAccount();

                        String exportString =
                                account.getUrl().replace("http", "webcal")
                                        + "calendar/export_execute.php"
                                        + "?userid=" + account.getUserId()
                                        + "&authtoken=" + token
                                        + "&preset_what=" + scope
                                        + "&preset_time=recentupcoming";

                        try {
                            Uri exportUri = Uri.parse(exportString);
                            Intent intent = new Intent(Intent.ACTION_VIEW, exportUri);

                            if (intent.resolveActivity(context.getPackageManager()) == null) throw new ActivityNotFoundException();
                            context.startActivity(intent);
                        } catch (ActivityNotFoundException e) {

                            ClipData clip = ClipData.newRawUri(
                                    context.getString(R.string.calendar_export_clipboard_label), Uri.parse(exportString.replace("webcal", "http"))
                            );
                            ((ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE))
                                    .setPrimaryClip(clip);

                            new AlertDialog.Builder(context)
                                    .setMessage(R.string.calendar_export_clipboard)
                                    .show();
                        }
                    },
                    alsoOnFailure -> export.setEnabled(true),
                    context
            );

        });
    }
}
