package godau.fynn.moodledirect.activity.fragment.module.forum;


import android.content.Context;
import android.os.Bundle;
import android.view.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionInflater;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.fragment.SwipeRefreshFragment;
import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.view.ClickListener;
import godau.fynn.moodledirect.view.adapter.forum.ForumAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ForumFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ForumFragment extends SwipeRefreshFragment {

    public static final String FORUM_ID_KEY = "forum_id";
    public static final String COURSE_NAME_KEY = "courseName";
    //private final int PER_PAGE = 20; TODO consider pagination

    private int forumId;
    private String courseName;

    private ForumAdapter adapter;

    private Context context;

    public static ForumFragment newInstance(int forumId, String courseName) {
        ForumFragment fragment = new ForumFragment();
        Bundle args = new Bundle();
        args.putInt(FORUM_ID_KEY, forumId);
        args.putString(COURSE_NAME_KEY, courseName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            forumId = getArguments().getInt(FORUM_ID_KEY);
            courseName = getArguments().getString(COURSE_NAME_KEY);
        }
        context = getContext();
        adapter = new ForumAdapter((parcel, position) -> {
            DiscussionFragment fragment = DiscussionFragment.newInstance(
                    parcel.discussion.discussionId, courseName
            );

            setSharedElementReturnTransition(TransitionInflater.from(getContext()).inflateTransition(R.transition.discussion));
            setExitTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.no_transition));

            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .addSharedElement(parcel.viewHolder.itemView, parcel.discussion.forumId + "card0")
                    .replace(((ViewGroup) getView().getParent()).getId(), fragment, "ForumDetail")
                    .commit();
        }, new ArrayList<>());
    }

    @Override
    public View onCreateContentView(@NonNull LayoutInflater inflater, ViewGroup container,
                                    Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forum, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = view.findViewById(R.id.forum_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(layoutManager);

        adapter.setStateRestorationPolicy(RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void loadData(MoodleDatabase.Dispatch dispatch) {
        ExceptionHandler.tryAndThenThread(
                () -> dispatch.getForum().getDiscussions(forumId),
                discussions -> {
                    swipeRefreshLayout.setRefreshing(false);

                    adapter.clearDiscussions();
                    if (discussions.isEmpty()) {
                        empty.text(R.string.no_posts_to_display);
                    } else {
                        empty.hide();
                        adapter.addDiscussions(discussions);

                    }
                },
                exception -> {
                    swipeRefreshLayout.setRefreshing(false);
                    empty.exception(exception);
                },
                context
        );
    }
}
