package godau.fynn.moodledirect.activity.fragment.module.forum;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionInflater;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.fragment.SwipeRefreshFragment;
import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.model.api.forum.Discussion;
import godau.fynn.moodledirect.module.link.ForumLink;
import godau.fynn.moodledirect.network.APIClient;
import godau.fynn.moodledirect.network.MoodleServices;
import godau.fynn.moodledirect.util.AutoLoginHelper;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.view.adapter.forum.DiscussionAdapter;

import java.util.ArrayList;
import java.util.List;

public class DiscussionFragment extends SwipeRefreshFragment {

    public static final String DISCUSSION_ID_KEY = "discussionId";
    public static final String FOLDER_NAME_KEY = "folderName";

    private String folderName;
    private int discussionId;

    private MoodleServices moodleServices;
    private final List<Discussion> discussion = new ArrayList<>();
    private DiscussionAdapter adapter;

    public static DiscussionFragment newInstance(int discussionId, String folderName) {
        DiscussionFragment fragment = new DiscussionFragment();
        Bundle args = new Bundle();
        args.putInt(DISCUSSION_ID_KEY, discussionId);
        args.putString(FOLDER_NAME_KEY, folderName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            discussionId = getArguments().getInt(DISCUSSION_ID_KEY);
            folderName = getArguments().getString(FOLDER_NAME_KEY);
        }
        if (moodleServices == null) {
            moodleServices = APIClient.getServices();
        }

        TransitionInflater inflater = TransitionInflater.from(requireContext());
        setSharedElementEnterTransition(inflater.inflateTransition(R.transition.discussion));

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateContentView(@NonNull LayoutInflater inflater, ViewGroup container,
                                    Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_discussion, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        postponeEnterTransition();
        super.onViewCreated(view, savedInstanceState);

        // DisplayMetrics for scaling images
        DisplayMetrics metrics = new DisplayMetrics();
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setChildDrawingOrderCallback((childCount, i) -> childCount - i - 1);
        adapter = new DiscussionAdapter(discussion, folderName, metrics);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void loadData(MoodleDatabase.Dispatch dispatch) {
        ViewGroup parentView = (ViewGroup) getView().getParent();

        ExceptionHandler.tryAndThenThread(
                () -> dispatch.getForum().getPostsByDiscussion(discussionId),
                discussions -> {
                    discussion.clear();
                    discussion.addAll(discussions);
                    adapter.notifyDataSetChanged();

                    swipeRefreshLayout.setRefreshing(false);

                    // Start the transition once all views have been measured and laid out
                    // https://developer.android.com/guide/fragments/animate#recyclerview
                    parentView.getViewTreeObserver()
                            .addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                                @Override
                                public boolean onPreDraw(){
                                    parentView.getViewTreeObserver()
                                            .removeOnPreDrawListener(this);
                                    startPostponedEnterTransition();
                                    return true;
                                }
                            });


                },
                onFailure -> swipeRefreshLayout.setRefreshing(false),
                requireContext()
        );
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.open_in_browser, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_open_in_browser) {
            AutoLoginHelper.openWithAutoLogin(requireContext(), getView(), ForumLink.getDiscussionUrl(discussionId));
            return true;
        } else return super.onOptionsItemSelected(item);
    }
}
