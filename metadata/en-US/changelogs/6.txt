Maintenance release with a small but helpful new feature that helps you integrate Moodle's calendar with your system calendar.

– Calendar link generation
– Bug fixes and translation updates
